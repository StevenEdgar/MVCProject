﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCProject.Models;
using System.Globalization;

namespace MVCProject.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie

        public ActionResult ListMovie()
        {
            Film mod = new Film();
            ViewBag.dataMovie = mod.GetMovies();
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(FormCollection col)
        {
            MovieEntities db = new MovieEntities();
            movie mov = new movie();
            Guid movieid = Guid.NewGuid();
            string namamovie = col["namamovie"];
            string genre = col["genre"];
            string tahunrilis = col["tahunrilis"];
            mov.movieid = movieid;
            mov.namamovie = namamovie;
            mov.genre = genre;
            mov.tahunrilis = tahunrilis;
            db.movies.Add(mov);
            db.SaveChanges();

            return RedirectToAction("ListMovie", "Movie");
        }

        public ActionResult Delete(Guid id)
        {
            MovieEntities db = new MovieEntities();
            Film flm = db.movies.Where(x => x.movieid == id).Select(x => new Film()
            {
                movieid = x.movieid,
                namamovie = x.namamovie,
                genre = x.genre,
                tahunrilis = x.tahunrilis
            }).SingleOrDefault();

            return View(flm);
        }

        [HttpPost]
        public ActionResult Delete(Film col)
        {
            MovieEntities db = new MovieEntities();
            movie mov = db.movies.Where(x => x.movieid == col.movieid).Single<movie>();
            db.movies.Remove(mov);
            db.SaveChanges();

            return RedirectToAction("ListMovie", "Movie");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            MovieEntities db = new MovieEntities();
            Film film = db.movies.Where(x => x.movieid == id).Select(x => new Film
            {
                movieid = x.movieid,
                namamovie = x.namamovie,
                genre = x.genre,
                tahunrilis = x.tahunrilis
            }).SingleOrDefault();

            return View(film);
        }

        public ActionResult Edit(Film col)
        {
            MovieEntities db = new MovieEntities();
            movie mov = db.movies.Where(x => x.movieid == col.movieid).Single<movie>();
            mov.movieid = col.movieid;
            mov.namamovie = col.namamovie;
            mov.genre = col.genre;
            mov.tahunrilis = col.tahunrilis;
            db.SaveChanges();
            return RedirectToAction("ListMovie", "Movie");
        }
    }
}