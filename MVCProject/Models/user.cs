﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Models
{
    public class user
    {
        public Guid Id { get; set; }
        public string fullname { get; set;}
        public string username { get; set; }
        public string password { get; set; }
        MovieEntities db = new MovieEntities();
        public List<systemuser> GetSystemUsers()
        {
            var users = (from x in db.systemusers
                         select x).ToList();
            return users;
        }
    }
}